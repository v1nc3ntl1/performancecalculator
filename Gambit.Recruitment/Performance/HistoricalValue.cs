﻿using System;

namespace Gambit.Recruitment.Business.Performance
{
    /// <summary>
    /// Represents a Historical value
    /// </summary>
    public class HistoricalValue
    {
        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public decimal Value { get; set; }
    }
}
