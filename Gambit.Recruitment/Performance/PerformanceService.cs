﻿using Gambit.Recruitment.Business.Common.Exception;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Gambit.Recruitment.Business.Performance
{
    /// <summary>
    /// Represents the Performance service responsible of computing the performances.
    /// </summary>
    public class PerformanceService
    {
        public readonly decimal DefaultPerformance = 0;

        /// <summary>
        /// Gets the performance between <paramref name="dateFrom"/> and <paramref name="dateTo"/> for the specified <paramref name="historicalValues"/>
        /// </summary>
        /// <param name="dateFrom">The date to use as start date to compute the performance.</param>
        /// <param name="dateTo">The date to use as end date to compute the performance.</param>
        /// <param name="historicalValues">The list of historical values which should be used to compute the performance.</param>
        /// <returns>The computed performance.</returns>
        public decimal GetPerformance(DateTime dateFrom, DateTime dateTo, IList<HistoricalValue> historicalValues)
        {
            // Check if historicalValues is empty
            // CHeck if historicalValues has at least two element
            // Check if HistoricalValue Date falls within dateFrom and dateTo
            // Check if historicalValue Value is greater than zero
            // Filter out non business day

            if (historicalValues == null || historicalValues.Count <= 1) throw new ArgumentException();

            if (dateFrom > dateTo) throw new InvalidDateRangeException();

            var orderedList = GetValidHistoricalValue(historicalValues, dateFrom, dateTo).OrderBy(x => x.Date);

            if (orderedList == null || orderedList.Count() <= 1) throw new ArgumentException();

            var p0 = orderedList.First().Value;
            var p1 = orderedList.Last().Value;

            if (p0 <= 0) return DefaultPerformance;

            var calculatedValue = (p1 - p0) / p0;
            return calculatedValue < 0 ? DefaultPerformance : calculatedValue;
        }

        public IEnumerable<HistoricalValue> GetValidHistoricalValue(IList<HistoricalValue> historicalValues, DateTime startDate, DateTime endDate)
        {
            foreach (var historicalValue in historicalValues)
            {
                // Assuming value cannot be negative
                if (historicalValue == null ||
                    historicalValue.Date == null ||
                    historicalValue.Value < 0) continue;

                if (IsWithinDateRange(historicalValue, startDate, endDate) &&
                    IsBusinessDay(historicalValue)) yield return historicalValue;
            }
        }

        private bool IsWithinDateRange(HistoricalValue value, DateTime startDate, DateTime endDate)
        {
            return value.Date >= startDate && value.Date <= endDate;
        }

        private bool IsBusinessDay(HistoricalValue value)
        {
            return value.Date.DayOfWeek != DayOfWeek.Saturday &&
                value.Date.DayOfWeek != DayOfWeek.Sunday;
        }
    }
}
