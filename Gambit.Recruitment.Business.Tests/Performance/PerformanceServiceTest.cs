﻿using Gambit.Recruitment.Business.Common.Exception;
using Gambit.Recruitment.Business.Performance;
using System;
using System.Collections.Generic;
using Xunit;

namespace Gambit.Recruitment.Business.Tests.Performance
{
    /// <summary>
    /// Represents the <see cref="PerformanceService"/>> test class.
    /// </summary>
    public class PerformanceServiceTest
    {
        private PerformanceService _testObj = new PerformanceService();

        [Fact]
        public void GetPerformance_HappyPath_ShouldReturnAValue()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() { new HistoricalValue() { Date = currentDate, Value = 20 }, new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 10 } };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);

            Assert.True(actual > 0);
        }

        // Check if historicalValues is empty
        [Fact]
        public void GetPerformance_WIthHistoricalValuesEmpty_ShouldThrowError()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() { };

            Assert.Throws<ArgumentException>(() => _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list));
        }

        [Fact]
        public void GetPerformance_WIthHistoricalValuesNull_ShouldThrowError()
        {
            var currentDate = DateTime.Now;

            Assert.Throws<ArgumentException>(() => _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, null));
        }

        [Fact]
        public void GetPerformance_WIthInvalidDateRange_ShouldThrowError()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() { new HistoricalValue() { Date = currentDate, Value = 20 }, new HistoricalValue() { Date = currentDate.AddDays(-1), Value = 10 } };

            Assert.Throws<InvalidDateRangeException>(() => _testObj.GetPerformance(currentDate, currentDate.AddDays(-365), list));
        }

        // CHeck if historicalValues has at least two element
        [Fact]
        public void GetPerformance_WIthOnlyOneElement_ShouldThrowError()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() { new HistoricalValue() { Date = currentDate, Value = 20 }, new HistoricalValue() { Date = currentDate.AddDays(-1), Value = 10 } };

            Assert.Throws<InvalidDateRangeException>(() => _testObj.GetPerformance(currentDate, currentDate.AddDays(-365), list));
        }

        // Check if historyValues is correct
        [Fact]
        public void GetPerformance_WithCorrectDateRange_ShouldReturncorrectCount()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate.AddDays(-6), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 10 },
                new HistoricalValue() { Date = currentDate, Value = 20 }, 
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (20 -  5) / 5 = 3
            Assert.True(actual  == 3m);
        }

        [Fact]
        public void GetPerformance_WithUnorderList_ShouldReturncorrectCount()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate, Value = 20 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 10 },
                new HistoricalValue() { Date = currentDate.AddDays(-6), Value = 5 }
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (20 -  5) / 5 = 3
            Assert.True(actual == 3m);
        }


        // Check if historicalValue Value is greater than zero
        [Fact]
        public void GetPerformance_WithP0EqualZero_ShouldReturnZero()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate, Value = 20 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 10 },
                new HistoricalValue() { Date = currentDate.AddDays(-6), Value = 0 }
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (20 -  5) / 5 = 3
            Assert.True(actual == 0);
        }

        [Fact]
        public void GetPerformance_WithCalculatedPerformanceLessThanZero_ShouldReturnZero()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate, Value = 0 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 10 },
                new HistoricalValue() { Date = currentDate.AddDays(-6), Value = 0 }
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (20 -  5) / 5 = 3
            Assert.True(actual == 0);
        }

        [Fact]
        public void GetPerformance_WithNonInclusiveDateRange_ShouldReturnCorrectValue()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate.AddDays(-3), Value = 10 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-367), Value = 10 }
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (10 -  5) / 5 = 2
            Assert.True(actual == 1);
        }
        [Fact]
        public void GetPerformance_WithNonInclusiveDateRange_ShouldReturnCorrectValue2()
        {
            var currentDate = DateTime.Now;

            var list = new List<HistoricalValue>() {
                new HistoricalValue() { Date = currentDate.AddDays(-3), Value = 10 },
                new HistoricalValue() { Date = currentDate.AddDays(-4), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-10), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-17), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-24), Value = 5 },
                new HistoricalValue() { Date = currentDate.AddDays(-367), Value = 10 }
            };

            var actual = _testObj.GetPerformance(currentDate.AddDays(-365), currentDate, list);
            // (10 -  5) / 5 = 2
            Assert.True(actual == 1);
        }
    }
}
